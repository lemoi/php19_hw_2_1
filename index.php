<?php
    /* Домашнее задание к лекции 2.1 «Установка и настройка веб-сервера»

    Необходимо вывести таблицу из JSON-файла в виде HTML-таблицы.
    JSON-файл хранит данные телефонной книжки.
    Пример JSON-файла:

    [{
      "firstName": "Иван",
      "lastName": "Иванов",
      "address": "г.Москва, ул. Алиева,2",
      "phoneNumber": "812 123-1234"
    }, {
      ...
    }]
    */

    $jsonText = file_get_contents(__DIR__ . '/users.json');
    $users = json_decode($jsonText, true);
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>PHP-19. Task 2.1</title>
</head>

<body>
    <table>
        <thead>
            <tr>
                <td>Имя</td>
                <td>Фамилия</td>
                <td>Адрес</td>
                <td>Телефон</td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user) { ?>
                <tr>
                    <td><?php echo $user["firstName"] ?></td>
                    <td><?php echo $user["lastName"] ?></td>
                    <td><?php
                        // Части адреса упорядочим одинаковым образом: город, улица, все остальное
                        $address = $user["address"];
                        $normalizedAddress = [];
                        $orderFields = ["city", "streetAddress"];
                        foreach($orderFields as $field) {
                            if (array_key_exists($field, $address)) {
                                $normalizedAddress[] = $address[$field];
                                unset($address[$field]);
                            }
                        }
                        $normalizedAddress = array_merge($normalizedAddress, $address);
                        echo implode(", ", $normalizedAddress);
                    ?></td>
                    <td><?php echo implode(", ", $user["phoneNumbers"]) ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</body>
</html>
